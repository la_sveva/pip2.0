# What's here
This directory hosts the scripts in charge of linking the source and output directories, which are meant to be stored outside of this repository, into the repository itself.

Indeed, the data may be saved on a different disk, possibly over a network service

# Conventions
The only rules to be followed is that in the top directory, three symlinks may be created:
- **chiavi** must contains all the ligands TODO: Check the format
- **serrature** must contains all the receptors TODO: Check the format
- **output** is where all the outputs of the pipeline are saved

Note: the three directories may be on three different disks/servers

# How to write an *experiment mounting* script
[template.sh](./template.sh) provides a simple template for writing such script, which is to be executed from this directory. A user may wants to write different scripts like this in order to perform docking simulations on different batch of data.

## Define the target symlinks
It is very **important** not to change these names as they are referred from all the software
```bash
chiaviFolder="../chiavi"
serratureFolder="../serrature"
outputFolder="../output"
```

## Remove existing links
When starting a new pipeline, it is necessary to remove previous links before creating new ones. This may be accomplished as follows:
```bash
[ -L $chiaviFolder ] && rm $chiaviFolder
[ -L $serratureFolder ] && rm $serratureFolder
[ -L $outputFolder ] && rm $outputFolder

```

## Create the new links
Before starting a new pipeline, the links must be created. This is how you can tell the software where the source files are located on your system and where you want the outputs to be stored. For the sake of simplicity, we placed all the three directoryes within a common top directory `${experimentDir}`. However, the source may be defined as the user prefer.
```bash
ln -vs ${experimentDir}/chiavi $chiaviFolder
ln -vs ${experimentDir}/serrature $serratureFolder
ln -vs ${experimentDir}/output $outputFolder
```

# Mounting the general directory
The ligands and receptors are specific of a given experiment, and therefore different experiments require different bulks of data. However, there may be some *general* files, that is, they are somehow common to all the pipeline, and may be stored in a different directory.

[linkGeneralFolder.sh](./linkGeneralFolder.sh) is in charge of creating this symlink. The only convention is this directory should be linked to `general` in the top directory.


# Scraping Auxiliary directories
When performing scrapping ~~of ligands (keys) from databases (TODO: Link script),~~ it may be convenient to define some auxiliary directories where the scraped files may be dumped. In fact, starting from this, a human may decide which files are worth to be placed inside a *chiavi* or *serrature* directories for a given virtual screening experiment. The [linkScraping.sh](./linkScraping.sh) script is responsible of creating the required directories. In particular, the following symlinks will be defined in the top directory of this project, that is
- **SDF_ZINC15_subsets**: All the sub-catalogues retrieved by [scrap_ZINC15_FULL.py](../utils/scraping/scrap_ZINC15_FULL.py) are placed under this directory
- **Alphafold**: All pdb files retrieved by [scrap_Alphafold.py](../utils/scraping/scrap_Alphafold.py) are placed under this directory

The source directory of each of these directories may be defined by the user. However, it is fundamental not to change the name of the symlinks as they are referenced by scripts

