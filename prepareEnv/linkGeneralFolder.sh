#!/bin/bash

# This script links the general archive folder where files that do not belong to a specific experiment, but rather are somehow, general, are stored

# Define here the basedir
rootPath="/data/pitoneverde"   

# Local folder inside repo, target of symlink
# DO NOT CHANGE!! This is referred by all the scripts
generalFolder="general"
local_general="../${generalFolder}"


# Remove symlink if exists 
[ -L $local_general ] && rm -v $local_general

# Create the symlink. The source may be located where preferred
ln -vs "${rootPath}/general_archive" $local_general
