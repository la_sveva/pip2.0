#!/bin/bash

# This script links the SDF data folders into the local repo
# Call from inside the folder prepareEnv
# The link is created in the repo root directory

# Define root path where data are saved
# Multiple slash in a path are threated as a single one


rootPath="/data/pitoneverde/"

# Define the suffixes
# DO NOT CHANGE!! These are referred by all the scripts
suff_ZINC15_subsets="ZINC15_subsets"
suff_Alphafold="Alphafold"


# Local folder inside repo, target of symlinks
# DO NOT CHANGE!! These are referred by all the scripts
local_ZINC15_subsets="../SDF_${suff_ZINC15_subsets}"
local_Alphafold="../${suff_Alphafold}"


# Delete symlinks if exists
[ -L $local_ZINC15_subsets ] && rm -v $local_ZINC15_subsets
[ -L $local_Alphafold ] && rm -v $local_Alphafold


# Create symlinkls
# Change here the first path if needed
ln -vs "${rootPath}/SDFnew_${suff_ZINC15_subsets}" $local_ZINC15_subsets
ln -vs "${rootPath}/${suff_Alphafold}" $local_Alphafold

