#!/bin/bash

# Define here the basedir of the experiment data directories
experimentDir="/home/pitoneverde/TIRITA"

# Define target directories
# DO NOT CHANGE!! These are referred by all the scripts
chiaviFolder="../chiavi"
serratureFolder="../serrature"
outputFolder="../output"

# Remove existing symlinks
[ -L $chiaviFolder ] && rm $chiaviFolder
[ -L $serratureFolder ] && rm $serratureFolder
[ -L $outputFolder ] && rm $outputFolder


echo -e "Creating symlinks..."

# If the base directory does not have the subdirs labelled as
# `chiavi`, `serrature` and `output`, adjust here
ln -vs ${experimentDir}/chiavi $chiaviFolder
ln -vs ${experimentDir}/serrature $serratureFolder
ln -vs ${experimentDir}/output $outputFolder

echo -e "Done!"
