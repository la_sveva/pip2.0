# Pipeline folder
This folder contanis all the scripts of the pipeline. The pipeline is meant to perform a Virtual Screening experiment by performing docking simulations by means of [Autodock Vina](https://vina.scripps.edu/)

# Prerequisites

## Input files
- Before executing the pipeline, ensure that the `chiavi` and `serrature` folders contains some ligands and receptors respectively **formatted in PDB**
- Such files may be fetched from online databases. the [scraping](../utils/scraping/) package contains some useful scripts
- If one has files in different formats, the [formatConversion](../utils/formatConversion/) package contains some useful scripts

- the `chiavi` and `serrature` folders, along with the `output` one are meant to be linked to some folders on the working machine or even on some network server. This way, the files within this repository are not touched. The [prepareEnv](../prepareEnv/) folder contains a set of scripts in order to set up all the required symbolic links

# Script lists

## [1_prepareChiavi.sh](./1_prepareChiavi.sh)
- Loop over each pdb ligand in the `chiavi` folder and produce the pdbqt in the same folder if not already there
- It relies on prepare_ligand4.py, which is provided by the MGLTools TODO: [a bit of doc](https://gitlab.com/la_sveva/all_instructions/-/blob/main/server_from_scratch.md?ref_type=heads#mgltools)
- This script requires the conda `py27` environment TODO [a bit of doc](https://gitlab.com/la_sveva/all_instructions/-/blob/main/server_from_scratch.md?ref_type=heads#mgltools)



## [2_prepareSerrature.sh](./2_prepareSerrature.sh)
Loop over each pdb receptor in the `serrature` folder and perform the following 4 steps:

1. Remove lines starting with `ATOM` and with the value in columns 61-66 smaller than 70. TODO: Check idx are correct
2. Remove lines starting with `HETATM`, namely keys already docked
3. - By means of `prepare_pdb_split_alt_confs.py`, it splits a file containing more molecules inside it (called A, B, C...) producing (input_A.pdb, input_B.pdb, input_C.pdb...)
    - Even if `prepare_pdb_split_alt_confs.py` is part of the MGLTools, it is not directly on the path. A workarond to make it available is 
        ```bash
        ln -s  ~/anaconda3/envs/py27/bin/../MGLToolsPckgs/AutoDockTools/Utilities24/prepare_pdb_split_alt_confs.py prepare_pdb_split_alt_confs.py
        ```
4. - Finally, the pre-processed **pdb** can be converted to **pdbqt** with prepare_receptor4.py` which is provided by the MGLTools
    - A different for loop is necessary as due to step 3. more pdb files may be present, namely the ones _A.pdb, _B.pdb...
    - In particular, if a file was splitted (and hence the _A version as created), the pdbqt will not be produced, as it is to be done only on sub-molecules 