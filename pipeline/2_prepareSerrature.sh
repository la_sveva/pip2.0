#!/bin/bash

# This script preprocess pdb files and convert them to pdbqt
# It relies on two python scripts provided by the MGLTools


# Activate conda py27 environment
eval "$(conda shell.bash hook)"
conda activate py27

# Navigate to the receptor folder
# Ensure to have linked with a script in prepareEnv
cd ../serrature


# Loop over each file and perform a pre-processing. 
# It is safe if called multiple times on the same folder 
echo "*** Pre-processing PDBs ***"
for serratura in $(ls *.pdb); do

	echo $serratura

    # 1. Remove lines starting with ATOM and with a number smaller than 70
    # in columns 61 to 66. TODO: Check it is correct
    tempFile=$(mktemp)  # Temp file
    trap 'rm -f "$tempfile"' EXIT # Remove on exit even if Ctrl-C 
    awk '{
        if ($0 ~ /^ATOM/) {
            temp = substr($0, 61, 6) + 0.0  # 61-66 to float
            if (temp >= 70) print $0 
        } else {
            print $0  
        }
    }' $serratura > $tempFile && mv $tempFile $serratura


    # 2. Remove rows starting with HETATM, namely keys already docked :)
	sed -i '/^HETATM/d' $serratura


    # 3. Split macro-files with multiple molecules inside (A, B, C)
	# A symlink was necessary 
	# ln -s  ~/anaconda3/envs/py27/bin/../MGLToolsPckgs/AutoDockTools/Utilities24/prepare_pdb_split_alt_confs.py prepare_pdb_split_alt_confs.py
	prepare_pdb_split_alt_confs.py -r $serratura

done


# 4. Create the pdbqt from pdb
# Now I might have more pdbs, because I have _A, _B...
# TODO: It threats the source of _A, _B... as an actual file...
echo "*** prepare_receptor4.py ***"
for serratura in $(ls *.pdb); do

	echo $serratura

    # If a file was splitted (and hence _A version was created), do not create pdbqt
    [ -f "${serratura%.pdb}_A.pdb" ] && continue

	# Convert PDB to PDBQT
	[ ! -f ${serratura}qt ] && prepare_receptor4.py -r $serratura -o ${serratura}qt -A checkhydrogens

done
