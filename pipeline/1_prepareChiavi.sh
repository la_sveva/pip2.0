#!/bin/bash

# This script converts each pdb to pdbqt by means of prepare_ligand4.py
# which is provided by the MGLTools


# Activate conda py27 environment
eval "$(conda shell.bash hook)"
conda activate py27

# Navigate to the ligand folder
# Ensure to have linked with a script in prepareEnv
cd ../chiavi


# Loop over each pdb
# Produces the corresponding pdbqt if not already there
for chiave in $(ls *.pdb); do
	[ ! -f ${chiave}qt ] && prepare_ligand4.py -l $chiave -v
done
