# Format conversion scripts
This package contains a set of scripts to perform format conversions

## [SDF_to_PDB.sh](./SDF_to_PDB.sh)
- By means of `OpenBabel`, it converts an SDF file, such as those fetched from ZINC15 to the PDB format, which is the format of the inputs of the pipeline
- It is basiccally a for loop and perform a call to `obabel` for each SDF file: output files will saved in the same directory with the same filename, with the extension replaced to `pdb`
- The directory to be processed is to be passed as the first command line argument. For instance,
    ```bash
    ./callObabel.sh ../../SDF_ZINC15_subsets/fda/
    ```

- OpenBabel is a multi purpose utlity: in fact, `-isdf -opdb` represents the input and output formats
- TODO: Describe how to install OpenBabel, which is in MGLTools [a bit of doc](https://gitlab.com/la_sveva/all_instructions/-/blob/main/server_from_scratch.md?ref_type=heads#mgltools)