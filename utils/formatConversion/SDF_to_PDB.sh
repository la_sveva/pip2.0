#!/bin/bash

# This scripts accepts as only argument a folder with sdf files
# pdb files will be created in the same folder

# Call as, for instance ./callObabel.sh ../../SDF_ZINC15_subsets/fda/
# If the folder is a symlink, ensure to create the link first (prepareeEnv/linkScraping.sh)

[ $# -lt 1 ] && echo "provide the working directory" && exit 1

myPath=$1
echo "Processing $myPath..\n"
[ ! -d "$myPath" ] && echo "Please provide a real directory"

for file in $(ls ${myPath}/*.sdf); do
    echo "$file ---> ${file%.sdf}.pdb"

    # Call obabel over each file
    obabel -isdf $file -opdb -O ${file%.sdf}.pdb 
