# What's here
Here some utility scripts, acting on general files, that is, resource files which may be shared between different virtual screening experiments, can be found.

- [knownInteractionsDatabase](./knownInteractionsDatabase/) is a package to build a database of known interaction ligand-receptor pairs
- [scraping](./scraping/) contains few scripts to *scrap*, that is, to fetch substances from various online databases
- [formatConversion](./formatConversion/) contains scripts to perform format conversions