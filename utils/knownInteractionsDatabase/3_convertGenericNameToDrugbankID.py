#!/usr/bin/env python3

"""
- This script allows to convert the ligand *Generic Name* from the output of step 1, to the Drugbak ID, through the map built at step 2

- Call: ./3_convertGenericNameToDrugbankID.py
"""

# ligant (generic name) - receptor (Uniprot ID) known interactions
inputFile = r"../../general/interactions_commonNames_UniprotIDs.txt"

# Generic Name -> 
translFile = r"../../general/fromGenericName_toDrugbankIDs.txt"

# ligant (Drugbank ID) - receptor (Uniprot ID) known interactions
outputFile = r"../../general/interactions_DrugbankIDs_UniprotIDs.txt"

# Not translated keys
notFoundFile = r"../../general/interactions_DrugbankIDs_UniprotIDs_notFound.txt"



# Dictionary for cleaning file names
replacementDict = {
    "(" : "",
    ")" : "",  
    "[" : "",   
    "]" : "",
    "{" : "",
    "}" : "",
    
    "/"  : "_",
    "\\" : "_",
    " "  : "_",
    "-"  : "_",
    "\n" : "",
    "\t" : "_",
    
    "\"" : "",
    "'"  : "",
    ","  : "_",
    "`"  : ""
}
transTable = str.maketrans(replacementDict)



# 1. Construct a dict, key = Generic Name, value = Drugbank ID
genericToDrugbank = {}

with open(translFile, "r") as f:
    for i, line in enumerate(f):
        
        if i==0: continue # Skip heaeder
        
        val, key = line.split("\t")
        key = key.translate(transTable).lower()
        genericToDrugbank[key] = val
        


# 2. Replace in input file
cntSuccess = 0
cntErr = 0
setNotFound = set()

with open(inputFile, "r") as inf, open(outputFile, "w") as outf:
    
    # Loop over each line in input file
    for line in inf:
        key, uniprot = line.split("\t")
        key = key.translate(transTable).lower()
        
        try:
            outf.write(genericToDrugbank[key])  # Translate key
            outf.write("\t")
            outf.write(uniprot)
            outf.write("\n")
            
            cntSuccess += 1
            
        except KeyError:
            print(f"Drugbank id not found for key: {key}")
            setNotFound.add(key)
            
            # Write original row if cannot convert to Drugbank ID
            outf.write(line)
            
            cntErr += 1

# Dump key not translated        
with open(notFoundFile, "w") as f:
    f.writelines("\n".join(setNotFound))
        
# Final report
print(f"Rows successfully replaced: {cntSuccess} [{cntSuccess/(cntSuccess+cntErr)*100:.2f}%]")
print(f"Rows not replaced: {cntErr} [{cntErr/(cntSuccess+cntErr)*100:.2f}%]")

print(f"Unique keys not found: {len(setNotFound)}")
