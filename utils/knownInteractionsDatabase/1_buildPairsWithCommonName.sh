#!/bin/bash

# This script produces the `interactions_cleaned.txt` in the general folder
# In particular, the output of this script is a list of ligand-receptor pairs with known interaction
# The key appears with the common name
# The lock appears with the UniprotID

# Source file obtained with
# $ wget https://unmtid-dbs.net/download/DrugCentral/2021_09_01/drug.target.interaction.tsv.gz --directory-prefix=../../general/
# $ gunzip ../../general/drug.target.interaction.tsv.gz

# Call as:
# ./1_buildPairsWithCommonName.sh
# ./1_buildPairsWithCommonName.sh <inFile>
# ./1_buildPairsWithCommonName.sh <inFile> <outFile>
# Ensure to have linked the general folder first (prepareEnv/linkGeneralFolder.sh)


inFile="../../general/drug.target.interaction.tsv"
outFile="../../general/interactions_commonNames_UniprotIDs.txt"

if [ $# -lt 1 ]; then
    echo "Using default input: --> ${inFile}"
    echo "Using default output: --> ${outFile}"
    
elif [ $# -ge 1 ]; then
    inFile=$1
    echo "Using user provided input: --> ${inFile}"

    if [ $# -ge 2 ]; then
        outFile=$2
        echo "Using user provided output: --> ${outFile}"
    else
        echo "Using default output: --> ${outFile}"
    fi
fi

[ ! -f $inFile ]  && echo "Please provide an existing input file"    && exit 1 



# 1. Remove quotation marks -> File will be separated by tabs
echo "Removing quotation marks..."

virgFile="${inFile}_filt"
tr -d '"' < $inFile > $virgFile
# sed -i 's/"//g' $1

echo "Done!"



# 2. Take 1st and 5th columns, if the last one is HOMO SAPIENS
echo "Extracting 1st and 5th columns..."

twoColumnFile="${inFile}_twoCol"
awk -F'\t' -v OFS='\t' '$NF == "Homo sapiens" {print $1, $5}' < $virgFile > $twoColumnFile

echo "Done!"



# 3. Manages rows with stick

# Indeed, a row may be formatted as follows
# acetyldigitoxin P05023|P05026|P13637|P14415|P50993|P54709|P54710|Q13733
# But we want to have pairs in the output, like
# acetyldigitoxin P05023
# acetyldigitoxin P05026
# acetyldigitoxin P13637
# ...
# It is way more efficient with awk


# Clean file
> $outFile

echo "Parsing row by row"

awk -F'\t' -v outFile="$outFile" '
BEGIN { OFS="\t" }
{
    # Get first element (before first tab)
    firstEl = $1
    
    # If second column features a "|", split into multiple lines
    if ($2 ~ /\|/) {
        n = split($2, arr, "|") 
        for (i = 1; i <= n; i++)
            print firstEl, arr[i] >> outFile 
    } else {
        # Print original row
        print $0 >> outFile
    }
}' "$twoColumnFile"


echo "Done!"

# Clean intermediate files
echo "Removing auxiliary files..."
rm -v $virgFile $twoColumnFile
echo "Done!"
