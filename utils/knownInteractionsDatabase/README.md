# knownInteractionsDatabase package 
This package contains a set of scripts to build a database of known interaction ligand-receptor pairs. More info about how to obtains the source files may be found in the very first rows of the scripts themselves, TODO: Decide if put something here

## **[1_buildPairsWithCommonName](./1_buildPairsWithCommonName.sh)**
- This script builds a list of known ligand-receptor pair interaction, separated by a *tab*
- The ligand appears with its **common name**, while the receptor with the **UniprotID**.
- If called without arguments, the input file is assumed to be `../../general/drug.target.interaction.tsv`, otherwise the first input is the input file to be used
- The output file is `../../general/interactions_commonNames_UniprotIDs.txt`, unless a custom output is specified through the second argument
- Ensure to have linked the general folder first [prepareEnv/linkGeneralFolder.sh](../../prepareEnv/linkGeneralFolder.sh)
- The parsing stage was rewritten from scratch in awk to improve performance


### Call
```bash
./1_buildPairsWithCommonName.sh
./1_buildPairsWithCommonName.sh <inFile>
./1_buildPairsWithCommonName.sh <inFile> <outFile>
```

### How to get source files
From the script directory, 
```bash
wget https://unmtid-dbs.net/download/DrugCentral/2021_09_01/drug.target.interaction.tsv.gz --directory-prefix=../../general/
gunzip ../../general/drug.target.interaction.tsv.gz
```
these commands allows to retrieve and unzip the drug target interaction database from DrugCentral


# [2_fromGenericName_toDrugbankIDs](./2_fromGenericName_toDrugbankIDs.sh)
- This script builds a two column list with the *generic name* and *Drugbank ID* for all the ligands on Drugbank
- If called without arguments, the input file is assumed to be `../../general/3D_structures.sdf`, otherwise the first input is the input file to be used
- The output file is `../../general/fromGenericName_toDrugbankIDs.txt`, unless a custom output is specified through the second argument
- Ensure to have linked the general folder first [prepareEnv/linkGeneralFolder.sh](../../prepareEnv/linkGeneralFolder.sh)
- The parsing stage was rewritten from scratch in awk to improve performance


### Call
```bash
./2_fromGenericName_toDrugbankIDs.sh
./2_fromGenericName_toDrugbankIDs.sh <inFile>
./2_fromGenericName_toDrugbankIDs.sh <inFile> <outFile>
```

### How to get source files
The source file may be retrieved from [https://go.drugbank.com/releases/latest#structures -> All (3D)](https://go.drugbank.com/releases/latest#structures). However, a registration is required, therefore it cannot be retrieved with a simple wget call.
The downloaded file was then placed by hands in the general folder



## **[3_convertGenericNameToDrugbankID](./3_convertGenericNameToDrugbankID.py)**
- This script allows to convert the ligand *Generic Name* from the output of step 1, to the Drugbak ID, through the map built at step 2
- As this script is a bit more sofisticated, it was written in Python
- At the moment, the input and outputs are hardcoded in the very first lines of the script itself




### Detailed description
- This script first loads in a dictionary the conversion from *Generic Name* to *Drugbank ID*. In order to better compare strings, the following replacements were performed to ger rid of parenthesis and other symbols. At the moment, this convertion is only inside the script itself, such names are not saved anywhere.
```python
replacementDict = {
    "(" : "",
    ")" : "",  
    "[" : "",   
    "]" : "",
    "{" : "",
    "}" : "",
    
    "/"  : "_",
    "\\" : "_",
    " "  : "_",
    "-"  : "_",
    "\n" : "",
    "\t" : "_",
    
    "\"" : "",
    "'"  : "",
    ","  : "_",
    "`"  : ""
}
```
Furthermore, strings were compared after a convrsion to lowercase `key.translate(transTable).lower()`
- This script loops over each line of the known ligand-receptor interaction list
- If possible, it replaces the generic name with the corresponding Drugbank ID in the output file (`../../general/interactions_DrugbankIDs_UniprotIDs.txt`)
- Otherwise,
    - The original line with the generic name is reported
    - The key not found is also reported in (`../../general/interactions_DrugbankIDs_UniprotIDs_notFound.txt`) to ease the investigation of the problems


### Call
```bash
./3_convertGenericNameToDrugbankID.py
```
At the moment, it is not possible to pass file names through command line arguments. They should be changed only through first lines of the script