#!/bin/bash



# Source file obtained with
# https://go.drugbank.com/releases/latest#structures -> All (3D)
# Link: https://go.drugbank.com/releases/5-1-13/downloads/all-3d-structures
# However, it cannot retrieved with wget as authentication is required

# Call as 
# ./2_fromGenericName_toDrugbankIDs.sh
# ./2_fromGenericName_toDrugbankIDs.sh <inFile>
# ./2_fromGenericName_toDrugbankIDs.sh <inFile> <outFile>
# Ensure to have linked the general folder first (prepareEnv/linkGeneralFolder.sh)



# Define the files
inFile="../../general/3D_structures.sdf"
outFile="../../general/fromGenericName_toDrugbankIDs.txt"

if [ $# -lt 1 ]; then
    echo "Using default input: --> ${inFile}"
    echo "Using default output: --> ${outFile}"
    
elif [ $# -ge 1 ]; then
    inFile=$1
    echo "Using user provided input: --> ${inFile}"

    if [ $# -ge 2 ]; then
        outFile=$2
        echo "Using user provided output: --> ${outFile}"
    else
        echo "Using default output: --> ${outFile}"
    fi
fi

[ ! -f $inFile ]  && echo "Please provide an existing input file"    && exit 1 



# Parse file, row by row
# Each section is divided by "$$$$"
# Within each section, we look for the rows > <DRUGBANK_ID> and > <GENERIC_NAME> and stores
# the following lines into internal variables, which are dumped into a file 
# It is way more efficient with awk

echo "Parsing row by row"

awk -v outFile="$outFile" '
BEGIN {
    print "DRUGBANK_ID\tGENERIC_NAME" > outFile  
    drugbank_id = ""; generic_name = ""
}
{
    if ($0 == "> <DRUGBANK_ID>") {
        getline drugbank_id
    } 
    else if ($0 == "> <GENERIC_NAME>") {
        getline generic_name
        print drugbank_id "\t" generic_name >> outFile
    } 
    else if ($0 == "$$$$") {
        drugbank_id = ""; generic_name = ""
    }
}' "$inFile"

echo "Done!"
