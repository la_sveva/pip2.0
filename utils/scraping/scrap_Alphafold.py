#!/usr/bin/env python3

# This script aims to obtain some of the Alphafold substances (serrature) 
# https://alphafold.ebi.ac.uk/
# In particular, the following API returns a pdb file
# https://alphafold.ebi.ac.uk/files/AF-{uniprot_id}-F1-model_v4.pdb


# The user can provide the UniprotIDs of the substances to be retrieved 
# either as a command line arguments or throug a file, which contains one
# UniprotID on each line
# This script does NOT scrap the whole database

# Downloaded pdb are placed in ""../../Alphafold as f"{uniprot_id}.pdb"
# Ensure to have linked the Alphafold folder first (prepareEnv/linkScraping.sh)


# Example of call:
# - ./scrap_Alphafold.py --list P01100 P01189 P08456
# - ./scrap_Alphafold.py --file list.txt


import argparse
import os
import subprocess


# Argparser to define receptors to be scraped 
# either via a file or command line arguments

parser = argparse.ArgumentParser(
                    prog="scrap_Alphafold",
                    description="Scraping script of Alphafold databse",
                    epilog="Text at the bottom of help")

group = parser.add_mutually_exclusive_group(required=True)


# Command line list of receptors to be scraped
group.add_argument("--list", 
                    nargs="+", 
                    type=str,
                    help="List of receptors to be scraped", 
                    )

# Text file with one receptor in each row
group.add_argument("--file", 
                    type=str, 
                    help="File with receptors to be scraped one per row"
                    )

args = parser.parse_args()


# Error file
errorFile = "./scrap_Alphafold.log"
try:
    os.remove(errorFile)
except OSError:
    pass


# Output folder
outDir = f"../../Alphafold"
if not os.path.isdir(outDir): os.mkdir(outDir)


# Define the list of receptors to be retrieved
listReceptors = []
if args.file:
    with open(args.file, "r") as f:
        for line in f:
            listReceptors.append(line.strip())
else:
    listReceptors = args.list


# Loop over each uniprotID
for uniprot_id in listReceptors:
    
    try:
        
        # Url of the pdb to be retrieved
        url = f"https://alphafold.ebi.ac.uk/files/AF-{uniprot_id}-F1-model_v4.pdb"
        
        lstCmd = [
            "wget",
            "-c",
            url,
            "-O",
            os.path.join(outDir, f"{uniprot_id}.pdb"),
            "--no-check-certificate",
        ]
                
        # Run the wget command
        subprocess.run(lstCmd)
        
    except:
        with open(errorFile, "a") as f:
            f.write(f"Cannot find Uniprot id --> {uniprot_id}\n")
        continue

    



