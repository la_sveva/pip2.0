# What's here
This folder contains a set of scripts to scrap online databases

## [scrap_ZINC15_FUL](./scrap_ZINC15_FULL.py)
- This script allows to scrap ligands (keys) from [ZINC15](https://zinc15.docking.org/)
- It retrieves a set of sub-catalogues, hard coded in the `lstSubsets` list
- Each catalogue is downloaded in a different subfolder, named after the catalogue itself
- Catalogues folders are located in the `SDF_ZINC15_subsets` folder, which should be linked in the project top directory. Ensured to have linked with [prepareEnv/linkGeneralFolder.sh](../../prepareEnv/linkScraping.sh) before running this scripts
- Each file is named with the **Drugbank ID**, which is found under the **DrugBank-approved** entry on the element page
- If the **DrugBank-approved** entry is not found, the download is skipped and the url  will be written in `./scrap_ZINC15_FULL.log`
- Regular expression patterns are compiled once to improve performances

### Relevant libraries
- **`BeautifulSoup`** to parse an HTML page
- **`requests`** to perform HTTP requests. This library is newer and with better performance than the lightweight `httplib2`
- **`subprocess`** to perform system calls. This is safer and more efficient wrt `os.system`


## [](./scrap_Alphafold.py)
- This script allows to scrap receptors (locks) from [Alphafold](https://alphafold.ebi.ac.uk/)
- In particular, the following API allows to get a pdb file from the corresponding **UniprotID**
```python
url = f"https://alphafold.ebi.ac.uk/files/AF-{uniprot_id}-F1-model_v4.pdb"
```
- The user can provide the **UniprotIDs** of the substances to be retrieved either as a command line arguments or throug a file, which contains one UniprotID on each line
- Two command line arguments are available: `--list` and `file`. Call as (for instance)
```bash
./scrap_Alphafold.py --list P01100 P01189 P08456
./scrap_Alphafold.py --file list.txt
```
- In the first case all the Uniprot IDs are provided in a row, while in the latter a text file with one UniprotID in each line can be indicated. The two arguments are mutually exlusive, and one is required
- Output files will be placed in the `Alphafold` folder, which should be linked in the project top directory.. Ensured to have linked with [prepareEnv/linkGeneralFolder.sh](../../prepareEnv/linkScraping.sh) before running this scripts
- Output files feature the following name pattern: `f"{uniprot_id}.pdb"`
- If a UniprotID is not found, the download is skipped and the ID  will be written in `./scrap_Alphafold.log`

### Relevant libraries
- **`requests`** to perform HTTP requests. This library is newer and with better performance than the lightweight `httplib2`
- **`subprocess`** to perform system calls. This is safer and more efficient wrt `os.system`
