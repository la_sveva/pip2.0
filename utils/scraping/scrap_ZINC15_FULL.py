#!/usr/bin/env python3

# This script aims to obtain some of the zinc15 substances (chiavi) subsets from
# https://zinc15.docking.org/substances/subsets/
# The subset of subsets is hardcoded below in lstSubsets

# Each catalog is saved in f"../../SDF_ZINC15_subsets/{nameCatalogue}"
# Each file is named with the **Drugbank ID**, which is found under the **DrugBank-approved** entry on the element page
# Ensure to have linked the SDF folder first (prepareEnv/linkScraping.sh)

import subprocess
import requests
import os, re
from bs4 import BeautifulSoup


# Define here the subsets to be scraped
lstSubsets = [
    # "in-cells-only",
    "fda",
    "world-not-fda",
    "investigational-only",
    "world",
    "in-trials",
    "in-vivo-only",
    "in-man-only",
    "in-man",
    "in-vivo",
    "in-cells",
    "in-vitro-only",
    "in-vitro",
]


# Dictionary for uniforming names 
# replacementDict = {
#     "(" : "",
#     ")" : "",
#     "[" : "",
#     "]" : "",
#     "{" : "",
#     "}" : "",
    
#     "/"  : "_",
#     "\\" : "_",
#     " "  : "_",
#     "-"  : "_",
#     "\n" : "",
#     "\t" : "_",
    
#     "\"" : "",
#     "'"  : "",
#     ","  : "_",
#     "`"  : ""
# }
# transTable = str.maketrans(replacementDict)


# Regular expression compiled once
re_substance_ZINC = re.compile('^/substances/ZINC')
re_drugbank_approved = re.compile(r"^\s*DrugBank-approved\s*$")

# Error file
errorFile = "./scrap_ZINC15_FULL.log"
try:
    os.remove(errorFile)
except OSError:
    pass


# Method to scrap a single catalogue
def scrapSingleCatalogue(nameCatalogue):
    
    # Output folder
    sdfDir = f"../../SDF_ZINC15_subsets/{nameCatalogue}"
    if not os.path.isdir(sdfDir): os.mkdir(sdfDir)
    
    print(f"Scraping {nameCatalogue} --> {sdfDir}")

    # Idx of page to scrap
    currPage = 1
    
    # Dict to handle duplicates
    duplicateDict = {}

    while(True):
        
        # Get a page of database
        try:
            url = rf"https://zinc15.docking.org/substances/subsets/{nameCatalogue}/?page={currPage}"
            response = requests.get(url, timeout=120) 
            response.raise_for_status()  # Exception if status is 4xx/5xx
            data = response.text  
        except Exception as e:
            with open(errorFile, "a") as f:
                f.write(f"Problem with url --> {urlElem}\n")
                f.write(f"{e}\n\n")                
            continue


        # Create html parser
        soup = BeautifulSoup(data, "html.parser")

        # Get all tag "a" whose link starts with "/substances/ZINC"
        lstPage = soup.find_all('a', href=re_substance_ZINC)
        
        # If this page has no elements, break
        if len(lstPage) == 0: break
        
        # Loop over database entries within this page
        for el in lstPage: 
            
            # # Get common name, if any
            # lstAbbr = el.findChildren("abbr")
            
            # if lstAbbr:
            #     commonName = lstAbbr[0].get("title")
            #     commonName = commonName.lower().translate(transTable) # Adjust file name according to dictionary
            # else:
            #     continue
            
            
            # Relative link to the element
            linkElem = el.get("href")

            
            #
            # Get Drugbank ID from each element page
            #
            
            # Get the element page
            try:
                urlElem = f"https://zinc15.docking.org{linkElem}"    
                responseElem = requests.get(urlElem, timeout=120) 
                responseElem.raise_for_status()  # Exception if status is 4xx/5xx
                dataElem = responseElem.text  
                
            except Exception as e:
                with open(errorFile, "a") as f:
                    f.write(f"Problem with url --> {urlElem}\n")
                    f.write(f"{e}\n\n")                
                continue


            # Create html parser
            soupElem = BeautifulSoup(dataElem, "html.parser")
            
            
            try:
                # Find link with DrugBank-approved as text
                approvedTagA = soupElem.find("a", string=re_drugbank_approved)
                
                # Find next <dd>, after the current <dt>
                ddTag = approvedTagA.find_parent("dt").find_next_sibling("dd")
            
                # Get DrugBank-approved, that is, Drugbank ID
                DrugbankID = ddTag.find("a").text.strip()
            except:
                with open(errorFile, "a") as f:
                    f.write(f"Cannot find Drugbank id --> {urlElem}\n")
                continue
                
            
            #
            # Download element & manage duplicate
            #
            
            # Build url for download 
            urlDownload = f"https://zinc15.docking.org{linkElem[:-1]}.sdf"    


            # If already in dict (is a duplicate)
            try:
                duplicateDict[DrugbankID] += 1
                
                # Download with wget, resuming partial download, specifying output file and progressive index
                # cmd = f"wget -c {urlDownload} -O {os.path.join(sdfDir, f'{DrugbankID}_{duplicateDict[DrugbankID]}.sdf')} --no-check-certificate"    
                # os.system(cmd) 
                lstCmd = [
                    "wget",
                    "-c",
                    urlDownload,
                    "-O",
                    os.path.join(sdfDir, f'{DrugbankID}_{duplicateDict[DrugbankID]}.sdf'),
                    "--no-check-certificate",
                ]

            # If first time 
            except KeyError as ke:
                duplicateDict[DrugbankID] = 1 
            
                # Download with wget, resuming partial download, specifying output file
                # cmd = f"wget -c {urlDownload} -O {os.path.join(sdfDir, f'{DrugbankID}.sdf')} --no-check-certificate"     
                # os.system(cmd) 
                lstCmd = [
                    "wget",
                    "-c",
                    urlDownload,
                    "-O",
                    os.path.join(sdfDir, f'{DrugbankID}.sdf'),
                    "--no-check-certificate",
                ]
                
            # Run the wget command
            subprocess.run(lstCmd)


        # Go to next page
        currPage += 1
            
            

# Call scraping function for all subsets
for f in lstSubsets: 
    scrapSingleCatalogue(f)
